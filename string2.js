function string2(str) {
    let ans = [];
    let buff = str.split(".");

    // check whether the entered string has 4 separate numbers
    if (buff.length !== 4) {
        // console.log("size exceeded");
        return [];
    }

    // check inside numbers for validity
    for (let i = 0; i < 4; i++) {

        // If there is any other character apart from number
        if (isNaN(buff[i] * 1)) {
            // console.log("not pure num");
            return [];
        }
        else {

            // The IPV4 is 8 bit integer 0-255
            if (buff[i] >= 0 && buff[i] < 256) {
                ans[i] = buff[i] * 1;
            }
            else {
                // console.log("more than 8 bit");
                return [];
            }

        }
    }

    return ans;
}



module.exports = string2;