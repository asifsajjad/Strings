function string3(str) {

    // created an array to map to month
    const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let buff = str.split("/");
    // chosen the first number before / which is number of month
    let mNum = buff[0];
    return month[mNum - 1];
}


module.exports = string3;