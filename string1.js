function string1(str)
{
    let buff="";
    
    // we check if the starting symbol is always either - or $
    if(!(str.charAt(0)==="$" || str.charAt(0)==="-"))
    {
        return 0;
    }
    // lets check if comma is at starting as the number cannot start with comma
    let numStart= str.indexOf('$')+1;
    if(str.charAt(numStart)===",")
    {
        return 0;
    }
    // remove the first $ sign
    buff=str.replace("$","");
    //Divide the string into 2 parts integer and decimal
    let buff1=buff.slice(0,buff.indexOf("."));
    let buff2=buff.slice(buff.indexOf("."),buff.length);

    buff1=buff1.replaceAll(",","");
    // now add first half and second half together in string
    buff=buff1+buff2;
    // multiply by 1 to make a num from string
    buff=buff*1;
    // if any extra symbol is present, the string will not become number
    if(isNaN(buff))
    {
        return 0;
    }
    return buff;
}


module.exports=string1;




/*  Below is the code to check for commas at specified point.
// find the length of the integer part to find the 3rd pos from last to remove comma
    let n_length=buff1.length;
    //  console.log("n_length",n_length);
    //remove commas from their destined places as per number rule
    for(let i=n_length-2;i>0;i=i-2)
    {
        // check is comma is present then remove and decrease the pointer to face the correct pos.
        if(buff1.charAt(i)===",")
        {
            temp=buff1.substring(0, i) +
            "" +
            buff1.substring(i + 1);
            buff1=temp;
            i--;
        }
        
    }
*/
// Test for  failing case

//   console.log(string1('-$1,21,11,052.021343'));