const string2= require('../string2');

function testString2(userStr){
    return string2(userStr);
}

const test= ['', '165.158.235.1',"''.155.21.1",'267.45.27.67','1234.234.234.123','1a2.234.255.1','255.124.234.155']

for(let item of test){
    console.log(testString2(item));
}