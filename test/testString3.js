const string3 = require('../string3');

function testString3(userStr) {
    return string3(userStr);
}

const test = ['10/15/2021', '1/25/2021', '11/5/2021', '5/14/2022', '03/15/2021',"''/12/2034"];

for (let i = 0; i < test.length; i++) {
    console.log(testString3(test[i]));
}