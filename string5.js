function string5(arr) {
    let res = '';
    if (!arr) {
        return '';
    }
    for (let i = 0; i < arr.length; i++) {
        // if any element inside the array is not a String
        if(typeof arr[i]!=='string'){
            return '';
        }
        // skip empty strings
        if(arr[i].length===0){
            continue;
        }
        res = res + " " + arr[i];
        // add full stop after last iteration
        if (i === arr.length - 1) {
            res = res + ".";
        }
    }
    return res;
}



module.exports = string5;