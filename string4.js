function string4(obj) {
    let first='',middle='',last='';
    // sliced off the first alphabet and capitalised and rest small case
    if (typeof obj.first_name === 'string' && obj.first_name.length!==0) {
    first = obj.first_name.slice(0, 1).toUpperCase() + obj.first_name.slice(1).toLowerCase()+' ';
    }
    
    // if middle name is there
    if (typeof obj.middle_name === 'string' && obj.middle_name.length!==0) {
        // also added space after middle name
        middle = obj.middle_name.slice(0, 1).toUpperCase() + obj.middle_name.slice(1).toLowerCase() + ' ';
    }

    if (typeof obj.last_name === 'string' && obj.last_name.length!==0) {
    last = obj.last_name.slice(0, 1).toUpperCase() + obj.last_name.slice(1).toLowerCase();
    }
    
    let res = first + middle + last;

    return res;
}



module.exports = string4;